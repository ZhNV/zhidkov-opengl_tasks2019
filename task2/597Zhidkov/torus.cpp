#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>
#include "TorusModel.hpp"
#include <Texture.hpp>
#include <LightInfo.hpp>

#include <iostream>
#include <vector>
#include <unistd.h>
#include <ctime>
/**
Куб и кролик. Управление виртуальной камерой. Вращение кролика.
*/


class SampleApplication : public Application
{
public:
    MeshPtr _torus;

    ShaderProgramPtr _shader;
    ShaderProgramPtr _markerShader;
    TexturePtr _worldTexture;
    GLuint _sampler;

    float torusR = 2.0f;
    float torusr = 0.5f;

    MeshPtr _marker; //Маркер для источника света

    //Координаты источника света
    float _lr = 2.0;
    float _phi = 0.0;
    float _theta = glm::pi<float>() * 0.25f;

    LightInfo _light;

    float shift = 0;
    clock_t lastClocks = 0;

    void update() override {
        Application::update();
        clock_t cl = clock();
        if (1.0 * (cl - lastClocks) / CLOCKS_PER_SEC > 0.05) {
            lastClocks = cl;
            _torus = makeTorus(torusR, torusr, N, shift);
            _torus->setModelMatrix(glm::mat4(1.0f));
            draw();
            shift += 0.01;
        }

    }

    void makeScene() override
    {
        Application::makeScene();

        _cameraMover = std::make_shared<FreeCameraMover>();

        //Создаем меш с тором
        _torus = makeTorus(torusR, torusr, N, 0.0f);
        _torus->setModelMatrix(glm::mat4(1.0f));

        //Создаем шейдерную программу        
        _shader = std::make_shared<ShaderProgram>("597ZhidkovData2/shader.vert", "597ZhidkovData2/shader.frag");

        _worldTexture = loadTexture("597ZhidkovData2/earth_global.jpg");
        glGenSamplers(1, &_sampler);
        glSamplerParameteri(_sampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glSamplerParameteri(_sampler, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);


        //Инициализация значений переменных освщения
        _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
        _light.ambient = glm::vec3(0.2, 0.2, 0.2);
        _light.diffuse = glm::vec3(0.8, 0.8, 0.8);
        _light.specular = glm::vec3(1.0, 1.0, 1.0);

        _marker = makeSphere(0.1f);
        _markerShader = std::make_shared<ShaderProgram>("597ZhidkovData2/marker.vert", "597ZhidkovData2/marker.frag");



    }




    void draw() override
    {
        Application::draw();

        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //Устанавливаем шейдер.
        _shader->use();

        //Устанавливаем общие юниформ-переменные
        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
        glm::vec3 lightPosCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_light.position, 1.0));

        _shader->setVec3Uniform("light.pos", lightPosCamSpace); //копируем положение уже в системе виртуальной камеры
        _shader->setVec3Uniform("light.La", _light.ambient);
        _shader->setVec3Uniform("light.Ld", _light.diffuse);
        _shader->setVec3Uniform("light.Ls", _light.specular);

        GLuint textureUnitForDiffuseTex = 0;

        if (USE_DSA) {
            glBindTextureUnit(textureUnitForDiffuseTex, _worldTexture->texture());
            glBindSampler(textureUnitForDiffuseTex, _sampler);
        }
        else {
            glBindSampler(textureUnitForDiffuseTex, _sampler);
            glActiveTexture(GL_TEXTURE0 + textureUnitForDiffuseTex);  //текстурный юнит 0
            _worldTexture->bind();
        }

        _shader->setIntUniform("diffuseTex", textureUnitForDiffuseTex);
        _shader->setMat4Uniform("modelMatrix", _torus->modelMatrix());


        _shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _torus->modelMatrix()))));
        _torus->draw();

        {
            _markerShader->use();

            _markerShader->setMat4Uniform("mvpMatrix", _camera.projMatrix * _camera.viewMatrix * glm::translate(glm::mat4(1.0f), _light.position));
            _markerShader->setVec4Uniform("color", glm::vec4(_light.diffuse, 1.0f));
            _marker->draw();
        }



        glBindSampler(0, 0);
        glUseProgram(0);

    }

    void updateTorus(int dN) {
        N += dN;
        _torus = makeTorus(torusR, torusr, N, shift);
        _torus->setModelMatrix(glm::mat4(1.0f));
        draw();
    }

    void handleKey(int key, int scancode, int action, int mods) override {
        Application::handleKey(key, scancode, action, mods);
        if (action == GLFW_PRESS) {
            if (key == GLFW_KEY_MINUS ) {
                updateTorus(-1);
            } else if (key == GLFW_KEY_EQUAL) {
                updateTorus(1);
            }
        }
    }

private:
    unsigned int N = 30;
};

int main()
{
    SampleApplication app;
    app.start();
    return 0;
}