#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>
#include "TorusModel.hpp"
#include <Texture.hpp>
#include <LightInfo.hpp>

#include <iostream>
#include <vector>
#include <unistd.h>
#include <ctime>

namespace
{
    float frand()
    {
        return static_cast<float>(rand()) / static_cast<float>(RAND_MAX);
    }

    //Удобная функция для вычисления цвета из линейной палитры от синего до красного
    glm::vec3 getColorFromLinearPalette(float value)
    {
        if (value < 0.25f)
        {
            return glm::vec3(0.0f, value * 4.0f, 1.0f);
        }
        else if (value < 0.5f)
        {
            return glm::vec3(0.0f, 1.0f, (0.5f - value) * 4.0f);
        }
        else if (value < 0.75f)
        {
            return glm::vec3((value - 0.5f) * 4.0f, 1.0f, 0.0f);
        }
        else
        {
            return glm::vec3(1.0f, (1.0f - value) * 4.0f, 0.0f);
        }
    }
}


class SampleApplication : public Application
{
public:
    MeshPtr _torus;

    ShaderProgramPtr _shader;
    ShaderProgramPtr _markerShader;
    TexturePtr _worldTexture;
    GLuint _sampler;

    float torusR = 2.0f;
    float torusr = 0.5f;

    MeshPtr _quad;

    ShaderProgramPtr _renderToGBufferShader;
    ShaderProgramPtr _renderDeferredShader;


    MeshPtr _lightSphere; //Маркер для источника света



    unsigned int _fbWidth = 1024;
    unsigned int _fbHeight = 1024;
    GLuint _framebufferId;

    GLuint _depthTexId;
    GLuint _normalsTexId;
    GLuint _diffuseTexId;

    int _Klights = 5;
    std::vector<LightInfo> _lights;

    float _attenuation0 = 1.0;
    float _attenuation1 = 0.0;
    float _attenuation2 = 0.35;

    void initFramebuffer()
    {
        //Создаем фреймбуфер
        glGenFramebuffers(1, &_framebufferId);
        glBindFramebuffer(GL_FRAMEBUFFER, _framebufferId);

        //----------------------------

        //Создаем текстуру, куда будет осуществляться рендеринг нормалей
        glGenTextures(1, &_normalsTexId);
        glBindTexture(GL_TEXTURE_2D, _normalsTexId);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB8, _fbWidth, _fbHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, 0);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, _normalsTexId, 0);

        //----------------------------

        //Создаем текстуру, куда будет осуществляться рендеринг диффузного цвета
        glGenTextures(1, &_diffuseTexId);
        glBindTexture(GL_TEXTURE_2D, _diffuseTexId);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB8, _fbWidth, _fbHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, 0);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, _diffuseTexId, 0);

        //----------------------------

        //Создаем текстуру, где будет находиться буфер глубины
        glGenTextures(1, &_depthTexId);
        glBindTexture(GL_TEXTURE_2D, _depthTexId);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32F, _fbWidth, _fbHeight, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);
        glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, _depthTexId, 0);

        //----------------------------

        //Указываем куда именно мы будем рендерить
        GLenum buffers[] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1 };
        glDrawBuffers(2, buffers);

        if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        {
            std::cerr << "Failed to setup framebuffer\n";
            exit(1);
        }

        glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }

    void makeScene() override
    {
        Application::makeScene();

        _cameraMover = std::make_shared<FreeCameraMover>();

        //=========================================================

        //Создаем меш с тором
        _torus = makeTorus(torusR, torusr, N, 0.0f);
        _torus->setModelMatrix(glm::mat4(1.0f));

        _quad = makeScreenAlignedQuad();
        _lightSphere = makeSphere(0.1f);

        //=========================================================
        //Инициализация шейдеров

        _renderToGBufferShader = std::make_shared<ShaderProgram>("597ZhidkovData3/togbuffer.vert", "597ZhidkovData3/togbuffer.frag");
        _renderDeferredShader = std::make_shared<ShaderProgram>("597ZhidkovData3/deferred.vert", "597ZhidkovData3/deferred.frag");


        _markerShader = std::make_shared<ShaderProgram>("597ZhidkovData3/marker.vert", "597ZhidkovData3/marker.frag");

        //=========================================================

        _worldTexture = loadTexture("597ZhidkovData3/earth_global.jpg");

        //=========================================================
        //Инициализация сэмплера, объекта, который хранит параметры чтения из текстуры
        glGenSamplers(1, &_sampler);
        glSamplerParameteri(_sampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glSamplerParameteri(_sampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);

        //=========================================================
        //Инициализация фреймбуфера для рендера G-буфераы

        initFramebuffer();


        //=========================================================
        float size = 2.0f;
        for (int i = 0; i < _Klights; i++)
        {
            LightInfo light;

            glm::vec3 color = getColorFromLinearPalette(frand());

            light.position = glm::vec3(frand() * size - 0.5 * size, frand() * size - 0.5 * size, frand() * 3.0);
            light.ambient = color * 0.1f;
            light.diffuse = color * 0.4f;
            light.specular = glm::vec3(0.5, 0.5, 0.5);
            light.attenuation0 = _attenuation0;
            light.attenuation1 = _attenuation1;
            light.attenuation2 = _attenuation2;

            _lights.push_back(light);
        }
    }


    void draw() override
    {
        drawToGBuffer(_camera);
        drawToScreen(_renderDeferredShader, _camera);
    }

    void drawToGBuffer(const CameraInfo& camera)
    {
        //=========== Сначала подключаем фреймбуфер и рендерим в текстуру ==========
        glBindFramebuffer(GL_FRAMEBUFFER, _framebufferId);

        glViewport(0, 0, _fbWidth, _fbHeight);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        _renderToGBufferShader->use();
        _renderToGBufferShader->setMat4Uniform("viewMatrix", camera.viewMatrix);
        _renderToGBufferShader->setMat4Uniform("projectionMatrix", camera.projMatrix);

        glActiveTexture(GL_TEXTURE0);  //текстурный юнит 0
        glBindSampler(0, _sampler);
        _worldTexture->bind();
        _renderToGBufferShader->setIntUniform("diffuseTex", 0);



        _renderToGBufferShader->setMat4Uniform("modelMatrix", _torus->modelMatrix());
        _renderToGBufferShader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(camera.viewMatrix * _torus->modelMatrix()))));
        _torus->draw();



        glUseProgram(0);
        glBindFramebuffer(GL_FRAMEBUFFER, 0); //Отключаем фреймбуфер
    }

    void drawToScreen(const ShaderProgramPtr& shader, const CameraInfo& camera)
    {
        //Получаем текущие размеры экрана и выставлям вьюпорт
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        //Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        shader->use();
        shader->setMat4Uniform("projMatrixInverse", glm::inverse(camera.projMatrix));

        glActiveTexture(GL_TEXTURE0);  //текстурный юнит 0
        glBindTexture(GL_TEXTURE_2D, _normalsTexId);
        glBindSampler(0, _sampler);
        shader->setIntUniform("normalsTex", 0);

        glActiveTexture(GL_TEXTURE1);  //текстурный юнит 1
        glBindTexture(GL_TEXTURE_2D, _diffuseTexId);
        glBindSampler(1, _sampler);
        shader->setIntUniform("diffuseTex", 1);

        glActiveTexture(GL_TEXTURE2);  //текстурный юнит 2
        glBindTexture(GL_TEXTURE_2D, _depthTexId);
        glBindSampler(2, _sampler);
        shader->setIntUniform("depthTex", 2);



        glDisable(GL_DEPTH_TEST);
        glEnable(GL_BLEND);
        glBlendFunc(GL_ONE, GL_ONE);

        //Параметры затухания сделаем общими для всех источников света
        shader->setFloatUniform("light.a0", _attenuation0);
        shader->setFloatUniform("light.a1", _attenuation1);
        shader->setFloatUniform("light.a2", _attenuation2);


        _quad->draw(); //main light

        for (int i = 0; i < _Klights; i++)
        {
            glm::vec3 lightPosCamSpace = glm::vec3(camera.viewMatrix * glm::vec4(_lights[i].position, 1.0));

            shader->setVec3Uniform("light.pos", lightPosCamSpace); //копируем положение уже в системе виртуальной камеры
            shader->setVec3Uniform("light.La", _lights[i].ambient);
            shader->setVec3Uniform("light.Ld", _lights[i].diffuse);
            shader->setVec3Uniform("light.Ls", _lights[i].specular);

            _quad->draw();
        }
        {
            _markerShader->use();
            for (int i = 0; i < _Klights; i++) {
                _markerShader->setMat4Uniform("mvpMatrix", _camera.projMatrix * _camera.viewMatrix * glm::translate(glm::mat4(1.0f), _lights[i].position));
                _markerShader->setVec4Uniform("color", glm::vec4(_lights[i].diffuse, 1.0f));
                _lightSphere->draw();
            }
        }

        glDisable(GL_BLEND);
        glEnable(GL_DEPTH_TEST);

        //Отсоединяем сэмплер и шейдерную программу
        glBindSampler(0, 0);
        glUseProgram(0);
    }

    void updateTorus(int dN) {
        N += dN;
        _torus = makeTorus(torusR, torusr, N, 0.f);
        _torus->setModelMatrix(glm::mat4(1.0f));
        draw();
    }

    void handleKey(int key, int scancode, int action, int mods) override {
        Application::handleKey(key, scancode, action, mods);
        if (action == GLFW_PRESS) {
            if (key == GLFW_KEY_MINUS ) {
                updateTorus(-1);
            } else if (key == GLFW_KEY_EQUAL) {
                updateTorus(1);
            }
        }
    }

private:
    unsigned int N = 30;
};

int main()
{
    SampleApplication app;
    app.start();
    return 0;
}