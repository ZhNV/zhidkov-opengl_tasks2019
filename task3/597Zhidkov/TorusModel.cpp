#include <Mesh.hpp>
#include <glm/simd/geometric.h>
#include <vector>



glm::vec3 transform_to_xyz(float R, float r, float phi, float theta)  {
    return glm::vec3((R + r * cos(phi)) * cos(theta), (R + r * cos(phi)) * sin(theta), r * sin(phi));
}

glm::vec3 normal(float R, float r, float phi, float theta) {
    glm::vec3 end = transform_to_xyz(R, r, phi, theta);
    glm::vec3 beg = glm::vec3(R * cos(theta), R * sin(theta), 0);
    glm::vec3 normal = glm::vec3(end-beg);
    return normalize(normal);
}

glm::vec2 texCoord(float phi, float theta, float shift) {
    float x = shift + theta / 2.0f / glm::pi<float>();
    return glm::vec2(x, phi / 2. / glm::pi<float>());
}

MeshPtr makeTorus(float R, float r, unsigned int N, float shift) {
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec2> texcoords;


    for (unsigned int i = 0; i < N; i++) {
        float theta = 2 * glm::pi<float>() * i / N;
        float theta1 = 2 * glm::pi<float>() * (i + 1) / N;
        for (unsigned int j = 0; j < N; j++) {
            float phi = 2 * glm::pi<float>() * j / N;
            float phi1 = 2 * glm::pi<float>() * (j + 1) / N;
            vertices.push_back(transform_to_xyz(R, r, phi, theta));
            vertices.push_back(transform_to_xyz(R, r, phi, theta1));
            vertices.push_back(transform_to_xyz(R, r, phi1, theta1));

            normals.push_back(normal(R, r, phi, theta));
            normals.push_back(normal(R, r, phi, theta1));
            normals.push_back(normal(R, r, phi1, theta1));

            texcoords.push_back(texCoord(phi, theta, shift));
            texcoords.push_back(texCoord(phi, theta1, shift));
            texcoords.push_back(texCoord(phi1, theta1, shift));

            vertices.push_back(transform_to_xyz(R, r, phi, theta));
            vertices.push_back(transform_to_xyz(R, r, phi1, theta1));
            vertices.push_back(transform_to_xyz(R, r, phi1, theta));

            normals.push_back(normal(R, r, phi, theta));
            normals.push_back(normal(R, r, phi1, theta1));
            normals.push_back(normal(R, r, phi1, theta));

            texcoords.push_back(texCoord(phi, theta, shift));
            texcoords.push_back(texCoord(phi1, theta1, shift));
            texcoords.push_back(texCoord(phi1, theta, shift));


        }

    }
    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

    DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf2->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());



    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);

    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());
    return mesh;
}